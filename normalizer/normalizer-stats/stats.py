# Copyright 2017-2018 Fawzi Mohamed
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from past.builtins import cmp
from builtins import map
from builtins import str
from builtins import range
from past.utils import old_div
import sys, re, json, glob, os
import setup_path
import fractions
from nomadcore.parse_streamed_dicts import readDict
from nomadcore.compact_sha import sha512
from nomadcore.json_support import addShasOfJson, jsonIndentS, jsonCompactS, jsonIndentF
from nomadcore.parser_backend import JsonParseEventsWriterBackend
from ase.data import chemical_symbols
from classify_structure import ClassifyStructure
from io import open
import numpy, numpy.linalg
import spglib
import logging, math

parseFun = re.compile(r"""\s*"(?P<meta>[-a-zA-Z0-9_]+)"\s*:\s*(?:"(?P<scalarStringVal>[^"]+)"|(?P<arrayVal>\[)|(?P<sectionLike>\[?\{)|(?P<baseVal>[-+0-9.a-zA-Z]*))""")

class RunInfo(object):
    """stats for a run"""
    def __init__(self, runUri, sets = None, counters = None, allMeta = None, classification = "NoClassification", formulaIds = None, symmInfo = None):
        self.runUri = runUri
        self.counters = {}
        if counters:
            self.counters.update(counters)
        self.sets = {}
        if sets:
            self.sets.update(sets)
        self.allMeta = set()
        if allMeta:
            self.allMeta.update(allMeta)
        self.classification = classification
        self.formulaIds = {}
        if formulaIds:
            self.formulaIds.update(formulaIds)
        if symmInfo:
            self.symmInfo = list(symmInfo)
        else:
            self.symmInfo = []

    def addToSets(self, k, v):
        oldV = self.sets.get(k)
        if oldV:
            oldV.add(v)
        else:
            self.sets[k]=set([v])

    def addMeta(self, metaName):
        self.allMeta.add(metaName)

    def addClassify(self, nFormula, cId):
        """add structure with id cId and class type cId"""
        oldIds = self.formulaIds.get(nFormula)
        if oldIds:
            oldIds.add(cId)
        elif nFormula:
            self.formulaIds[nFormula] = set([cId])
        else:
            logging.warn("formula was not defined in %s", self.runUri)
            self.formulaIds[""] = set([cId])

    def xcType(self):
        xcFunctional = self.sets.get("xc_functional_name", [])
        if "HF_X" in xcFunctional:
            if len(xcFunctional) > 1:
                fType = "hybrid"
            else:
                fType = "HartreeFock"
        elif any(x.startswith("HYB_") for x in xcFunctional):
            fType = "hybrid"
        elif any(x.startswith("GGA_K_") for x in xcFunctional):
            fType = "meta-GGA"
        elif any(x.startswith("MGGA_") for x in xcFunctional):
            fType = "meta-GGA"
        elif any(x.startswith("GGA_") for x in xcFunctional):
            fType = "GGA"
        elif any(x.startswith("LDA_") for x in xcFunctional):
            fType = "LDA"
        else:
            fType = "Unspecified"
        return fType

    def hasSome(self):
        return (self.sets or self.counters or self.allMeta or self.formulaIds or self.symmInfo)

    def __str__(self):
        return jsonIndentS(self.toDict())

    def writeStats(self, backend):
        backend.openContext(self.runUri)
        backend.addValue("xc_functional_type", self.xcType())
        backend.closeContext(self.runUri)

    def increment(self, meta, amount = 1):
        self.counters[meta] = self.counters.get(meta, 0) + amount

    def toDict(self):
        return {
            'type': 'RunInfo',
            'counters': self.counters,
            'sets': self.sets,
            'allMeta': self.allMeta,
            'formulaIds': self.formulaIds,
            'symmInfo' : json.loads(jsonCompactS(self.symmInfo)),
            'classification': self.classification,
            'xcType': self.xcType()
        }

class StatsInfo(object):
    """collect statistics for a calculation, an archive (or even across archives)

    tree is electronic structure method > XC type > classification > ( space group if bulk >) nFormula > unique ids & runs & single points
    """
    def __init__(self, contextUri):
        self.contextUri = contextUri
        self.sets = {}
        self.allMeta = set()
        self.tree = {}
        self.counters = {}

    def increment(self, meta, amount = 1):
        """increments a counter"""
        self.counters[meta] = self.counters.get(meta, 0) + amount

    def addRunToTree(self, runStats):
        if not runStats.hasSome():
            return
        for k,v in runStats.sets.items():
            oldV = self.sets.get(k)
            if oldV:
                oldV.update(v)
            else:
                self.sets[k] = set(v)
        self.allMeta.update(runStats.allMeta)
        methods = runStats.sets.get("electronic_structure_method",['other'])
        if runStats.symmInfo:
            cSys = crystalSystem(runStats.symmInfo[-1]['number'])
        else:
            cSys = 'undefined'
        classification = runStats.classification
        amounts = { 'class':'Amounts', 'formulaIds': runStats.formulaIds }
        for k, v in runStats.counters.items():
            if not k.startswith('n_'):
                amounts['n_' + k] = v
                amounts['r_' + k] = float(v) / float(len(methods))
            else:
                amounts[k] = v
                amounts['r' + k[1:]] = float(v) / float(len(methods))
        xcType = runStats.xcType()
        codes = runStats.sets.get('program_name')
        if not codes:
            pId = runStats.sets['parser_id'].pop()
            codes = [pId[:pId.index('Parser')]]
        else:
            codes = sorted(list(codes))
        if len(codes) != 1:
            raise Exception("several codes in %s: %s" % (runStats, codes))
        code = codes[0]
        newTree = { 'class': 'Code', code: amounts }
        if classification == 'Bulk':
            newTree = { "class": "Bravais Lattice", cSys: newTree }
        newTree = { "class": "XC", xcType: { 'class': 'Phase', classification: newTree } }
        for method in methods:
            mergeTree(self.tree, { "class": "Method", method: newTree })

    def addStats(self, stats):
        for k, v in stats.counters.items():
            self.counters[k] = self.counters.get(k, 0) + v
        for k,v in stats.sets.items():
            oldV = self.sets.get(k)
            if oldV:
                oldV.update(v)
            else:
                self.sets[k] = set(v)
        self.allMeta.update(stats.allMeta)
        mergeTree(self.tree, stats.tree)

    @staticmethod
    def writeTree(backend, tree, parentIdx):
        myIdx = backend.openSection("section_stats_tree_entry")
        if parentIdx is not None:
            backend.addValue("stats_tree_entry_parent", parentIdx)
        for k,v in tree.items():
            if isinstance(v, dict):
                return StatsInfo.writeTree(backend, v, myIdx)
            elif isinstance(v, set):
                setdx = backend.openSection("section_stats_tree_entry_set_value")
                backend.addValue("stats_tree_entry_set_name", k)
                for vv in sorted(v):
                    backend.addValue("stats_tree_entry_set_strValue", vv)
                backend.closeSection("section_stats_tree_entry_set_value", setdx)
            elif isinstance(v, int) or isinstance(v, float):
                backend.openSection("section_stats_tree_entry_counter")
                backend.addValue("stats_tree_entry_counter_name", k)
                backend.addValue("stats_tree_entry_counter_value", v)
                backend.openSection("section_stats_tree_entry_counter")
            elif isinstance(v, type("")) or isinstance(v, type(u"")):
                if k in ["class", "name", "description"]:
                    backend.addValue("stats_tree_entry_" + k, tree.get(k))
                else:
                    logging.warn("unexpected key value pair in tree: %s -> %s" % (k,v))
            else:
                raise Exception("Unexpected type %s found when writing %s -> %s" % (type(v), k, v))
        backend.closeSection("section_stats_tree_entry", myIdx)

    def writeStats(self, backend):
        backend.openContext(self.contextUri)
        backend.openNonOverlappingSection("section_stats")
        for k, v in sorted(self.counters.items()):
            backend.openNonOverlappingSection("section_stats_counter")
            backend.addValue("stats_counter_name", k)
            backend.addValue("stats_counter_value", v)
            backend.closeNonOverlappingSection("section_stats_counter")
        for k, v in sorted(self.sets.items()):
            backend.openNonOverlappingSection("section_stats_values")
            backend.addValue("stats_values_meta_name", k)
            for vv in sorted(v):
                backend.addValue("stats_values_value", vv)
            backend.closeNonOverlappingSection("section_stats_values")
        for meta in sorted(self.allMeta):
            backend.addValue("stats_meta_present", meta)
        backend.openNonOverlappingSection("section_stats_tree")
        self.writeTree(backend, self.tree, None)
        backend.closeNonOverlappingSection("section_stats_tree")
        backend.closeNonOverlappingSection("section_stats")
        backend.closeContext(self.contextUri)

    def __str__(self):
        return jsonIntentS(self.toDict())

    def toDict(self):
        return {
            'contextUri': self.contextUri,
            'type': 'StatsInfo',
            'counters': self.counters,
            'sets': self.sets,
            'allMeta': self.allMeta,
            'tree': self.tree
        }

    @staticmethod
    def listsToSet(tree):
        "recursive in-place replacement of lists with sets in a dictionary"
        for k,v in tree.items():
            if isinstance(v, list):
                tree[k] = set(v)
            elif isinstance(v, dict):
                StatsInfo.listsToSet(v)
        return tree

    @classmethod
    def fromDict(cls, d, contextUri = None):
        """initializes a Stats object with a dictionary coming from a json deserialization"""
        res = cls(contextUri if contextUri is not None else d.get("contextUri"))
        res.counters = d.get('counters', {})
        res.sets = cls.listsToSet(d.get('sets', {}))
        res.allMeta = set(d.get('allMeta', []))
        res.tree = cls.listsToSet(d.get('tree',{}))
        return res

def crystalSystem(spaceGroupNr):
    """returns the name of the crystal system (or bravais lattice) that contains the given space group"""
    if 1 <= spaceGroupNr <= 2:
        return "triclinic"
    elif 3 <= spaceGroupNr <= 15:
        return "monoclinic"
    elif 16 <= spaceGroupNr <= 74:
        return "orthorhombic"
    elif 75 <= spaceGroupNr <= 142:
        return "tetragonal"
    elif 143 <= spaceGroupNr <= 167:
        return "trigonal"
    elif 168 <= spaceGroupNr <= 194:
        return "hexagonal"
    elif 195 <= spaceGroupNr <= 230:
        return "cubic"
    else:
        raise Exception("invalid group number %s" % spaceGroupNr)


def toAtomNr(str):
    "returns the atom number of the given symbol"
    baseStr = str[:3].title()
    if baseStr.startswith("Uu") and baseStr in chemical_symbols[1:]:
        return chemical_symbols.index(baseStr)
    if baseStr[:2] in chemical_symbols[1:]:
        return chemical_symbols.index(baseStr[:2])
    elif baseStr[:1] in chemical_symbols[1:]:
        return chemical_symbols.index(baseStr[:1])
    else:
        return 0

def normalizedLabel(atomNr):
    """returns normalized label names from atom numbers.

    Ensures that the order for undefined species is preserved:
    the label of more negative numbers comes later in alphabetic ordering."""
    if atomNr > 0:
        return chemical_symbols[atomNr]
    else:
        alpha="abcdfghijklmnopqrstuvwxy" # missing 'e' as Xe is a valid chemical symbol, and 'z' to encode larger numbers
        s = ""
        ii = -k
        while ii > 0:
            r = (ii % len(alpha))
            s += alpha(r)
            ii = ii // len(alpha)
        if len(s) > 1:
            # z, nr of digits, number starting with largest exponent first (big endian ;)
            s += alpha[len(s)] # limits number to 24**24-1 which is (much) bigger than 2**31 :)
            s += "z"
            s = s[::-1]
        return 'X' + s

def dictToFormula(fDict, reduce = False):
    "transform a formula in dictionary form to a normalized string form"
    formula = ""
    nUndefDigits = None
    if reduce:
        nrs = list(fDict.values())
        if nrs:
            gcd = nrs[0]
            for n in nrs[1:]:
                gcd = fractions.gcd(gcd, n)
        else:
            gcd = 1
    else:
        gcd = 1
    for k,v in sorted(fDict.items()):
        if k > 0:
            formula += chemical_symbols[k]
        else:
            formula += 'X'
            alpha="bcdfghijklmnopqrstuvwxyz"
            if nUndefDigits is None:
                nVal = 1-min(fDict.keys())
                nUndefDigits = int(math.ceil(math.log(nVal)/math.log(len(alpha))))
            ii = -k
            s = ""
            while ii > 0:
                r = (ii % len(alpha))
                s += alpha(r)
                ii = ii // len(alpha)
            if s:
                s = ((nUndefDigits-len(s)) * "a") + s
            formula += s
        vv = v // gcd
        if vv > 1:
            formula += str(vv)
    return formula

calcPathRe = re.compile(r".*/(?P<archiveGid>R[-_a-zA-Z0-9]{28})/(?P<calculationGid>P[-_a-zA-Z0-9]{28})\.json$")

def statsOfFile(filePath, backend):
    """Extracts the statistics of a file and performs some normalizations

    emits to the backend the normalizations performed on the configurations (section system), and section_run."""
    m = calcPathRe.match(filePath)
    if not m:
        raise Exception("Unexpected path %r in statsOfFile, cannot extract archiveGid and calculationGid" % filePath)
    archiveGid = m.group("archiveGid")
    calculationGid = 'C' + m.group("calculationGid")[1:]
    statsInfo = StatsInfo("nmd://%s/%s" % (archiveGid, calculationGid))
    headerInfo = {}

    with open(filePath, encoding="utf-8") as fIn:
        toLift = set(["xc_functional_name", "electronic_structure_method", "program_name"])

        runInfo = None # in theory we should never add to it before find a section_run...

        atomLabels = None
        atomSpecies = None
        atomPos = None
        atomNLabels = None
        periodicDirs = None
        cell = None
        classType = "NoClassification"

        formula = None
        nFormula = None
        rFormula = None

        while True:
            line = fIn.readline()
            if not line: break
            m = parseFun.match(line)
            if not m:
                continue
            metaName = m.group("meta")
            if metaName == "parserId":
                headerInfo["parser_id"] = m.group("scalarStringVal")
            elif metaName == "mainFileUri":
                headerInfo["main_file_uri"] = m.group("scalarStringVal")
            elif metaName == "sections":
                break
        toIgnore = set(["sections","type","gIndex", "name"])
        sectionRe = re.compile(r"(?P<sectionName>[a-zA-Z0-9_]+)-(?P<sectionGIndex>[0-9]+)$")
        while True:
            line = fIn.readline()
            if not line: break
            m = parseFun.match(line)
            if not m:
                continue
            metaName = m.group("meta")
            if metaName in toIgnore:
                continue
            if m.group("sectionLike"):
                mm = sectionRe.match(metaName)
                if mm:
                    sectionName = mm.group("sectionName")
                    sectionGIndex = int(mm.group("sectionGIndex"))
                    if sectionName == "section_run":
                        if runInfo and runInfo.hasSome():
                            runInfo.writeStats(backend)
                            statsInfo.addRunToTree(runInfo)
                        runInfo = RunInfo("nmd://%s/%s/section_run/%dc" % (archiveGid, calculationGid, sectionGIndex))
                        runInfo.increment(sectionName)
                        atomLabels = None
                        atomSpecies = None
                        atomPos = None
                        atomNLabels = None
                        periodicDirs = None
                        cell = None
                        classType = "NoClassification"

                        formula = None
                        nFormula = None
                        rFormula = None
                        for k, v in headerInfo.items():
                            runInfo.addToSets(k, v)
                    runInfo.addMeta(sectionName)
                    if sectionName == "section_system":
                        runInfo.increment(sectionName)
                        conf = readDict(fIn,"{")
                        lab = conf.get("atom_labels", None)
                        periodicDirs = conf.get("configuration_periodic_dimensions", periodicDirs)
                        atomSpecies = conf.get("atom_atom_numbers", None)
                        if lab:
                            atomLabels = lab
                            if not atomSpecies:
                                atomSpecies = [toAtomNr(x) for x in atomLabels]
                        if atomSpecies:
                            formulaDict = {}
                            for el in atomSpecies:
                                formulaDict[el] = formulaDict.get(el, 0) + 1
                            formula = dictToFormula(formulaDict)
                            rFormula = dictToFormula(formulaDict, reduce = True)
                            atomNLabels = [normalizedLabel(x) for x in atomSpecies]
                            if periodicDirs is not None and any(periodicDirs):
                                nFormula = rFormula
                            else:
                                nFormula = formula
                        cell = conf.get("simulation_cell", cell)
                        pos = conf.get("atom_positions", None)
                        nConf = { }
                        if pos:
                            nConf['atom_positions'] = pos
                            atomPos = pos
                            if not formula:
                                if len(pos) != 1:
                                    formula = 'X' + len(pos)
                                else:
                                    formula = 'X'
                        if atomSpecies:
                            nConf['atom_species'] = atomSpecies
                        if cell:
                            nConf['lattice_vectors'] = cell
                        if periodicDirs:
                            nConf['configuration_periodic_dimensions'] = periodicDirs[0]
                        cId = 's' + addShasOfJson(nConf).b64digests()[0][0:28]
                        if classType == "NoClassification" and cell and atomLabels:
                            if cell:
                                nConf['simulation_cell'] = cell
                            if atomLabels:
                                nConf['atom_labels'] = atomLabels
                            nConf['name'] = 'section_system'
                            nConf['gIndex'] = sectionGIndex
                            structure = ClassifyStructure(None, jsonValue = {
                                "sections": [{
                                    "name": "section_run",
                                    "gIndex": 1,
                                    "sections":[nConf]
                                }]
                            })
                            classification = structure.classify()
                            if classification.get('classificationStatus', None) == 'ClassificationSuccess':
                                classType = classification['sections'][0]['sections'][0]['structure_kind']
                                runInfo.classification = classType
                            else:
                                classType = 'NoClassification'
                        runInfo.addClassify(nFormula, cId)
                        symm = None
                        if classType == 'Bulk' and (not runInfo.symmInfo) and atomPos and atomSpecies and cell:
                            acell = numpy.asarray(cell)*1.0e10
                            cellInv = numpy.linalg.inv(cell)
                            symm = spglib.get_symmetry_dataset((acell, numpy.dot(atomPos,cellInv), atomSpecies),
                                                     0.002, -1) # use m instead of Angstrom?
                            if symm:
                                runInfo.addToSets("spacegroup_3D_number", str(symm["number"]))
                                symm['configuration_raw_gid'] = cId
                                runInfo.symmInfo.append(symm)

                        sectionUri = "nmd://%s/%s/section_run/section_system/%dc" %(archiveGid, calculationGid,sectionGIndex)
                        backend.openContext(sectionUri)
                        backend.addValue("configuration_raw_gid", cId)
                        backend.addValue("atom_species", atomSpecies)
                        if symm:
                            for quantity in ["number", "international", "hall", "choice", "pointgroup"]:
                                v = symm.get(quantity)
                                if v is not None:
                                    backend.addValue("spacegroup_3D_" + quantity, v)
                            for quantity in ["transformation_matrix"]:
                                v = symm.get(quantity)
                                if v is not None:
                                    backend.addArrayValues("spacegroup_3D_" + quantity, numpy.asarray(v))
                            n = symm.get("number")
                            if n and crystalSystem:
                                backend.addValue("bravais_lattice", crystalSystem(n))
                            backend.addValue("chemical_composition", formula)
                            backend.addValue("chemical_composition_reduced", rFormula)
                            backend.addValue("chemical_composition_bulk_reduced", nFormula)
                            for quantity in ["origin_shift", "std_lattice"]:
                                v = symm.get(quantity)
                                if v is not None:
                                    backend.addArrayValues("spacegroup_3D_" + quantity, 1.0e-10 * numpy.asarray(v, dtype = float))
                            for (r,t) in zip(symm.get("rotations", []), symm.get("translations", [])):
                                backend.openNonOverlappingSection("section_spacegroup_3D_operation")
                                backend.addArrayValues("spacegroup_3D_rotation", numpy.asarray(r))
                                backend.addArrayValues("spacegroup_3D_translation", 1.0e-10 * numpy.asarray(t, dtype=float))
                                backend.closeNonOverlappingSection("section_spacegroup_3D_operation")
                            v = symm.get("wyckoffs")
                            logging.debug("symm: %s, wickoffs: %s", symm, v)
                            if v is not None:
                                for w in v:
                                    backend.addValue("spacegroup_3D_wyckoff", w)
                        backend.closeContext(sectionUri)
                    elif sectionName in ["section_k_band", "section_eigenvalues", "section_single_configuration_calculation"]:
                        runInfo.increment(sectionName)
                else:
                    if runInfo is None:
                        logging.warn("ignoring metaname %r before section_run" % metaName)
                    else:
                        runInfo.addMeta(metaName)
            else:
                runInfo.addMeta(metaName)
                if metaName in toLift:
                    runInfo.addToSets(metaName, m.group("scalarStringVal"))
    if runInfo.hasSome():
        runInfo.writeStats(backend)
        statsInfo.addRunToTree(runInfo)
    return statsInfo

def archiveStats(readArchiveSetDir, archiveGid, backend):
    if not glob.glob(os.path.join(readArchiveSetDir, "*", archiveGid)):
        logging.warn("No directory for archive %s, %s failed" % (archiveGid, os.path.join(readArchiveSetDir, "*", archiveGid)))
    parsedFiles = glob.glob(os.path.join(readArchiveSetDir, "*", archiveGid, "*.json"))
    nFailed = len(glob.glob(os.path.join(readArchiveSetDir, "*", archiveGid, "*.failed"))) + len(glob.glob(os.path.join(readArchiveSetDir, "*", archiveGid, "*.exception")))
    nParsed = len(parsedFiles)
    allStats = StatsInfo("nmd://" + archiveGid)
    allStats.increment("n_parsed", nParsed)
    allStats.increment("n_failed", nFailed)
    for f in parsedFiles:
        newStats = statsOfFile(f, backend)
        allStats.addStats(newStats)
    return allStats

def formulaToDict(formula):
    nameRe=re.compile(r"([A-Z][a-z]?[a-z]?)")
    components = nameRe.split(formula)
    formulaDict = {}
    for i in range(len(components) // 2):
        nStr = components[2 * i + 2]
        if nStr:
            n = int(nStr)
        else:
            n = 1
        formulaDict[components[2 * i + 1]] = n
    return formulaDict

def saveArchiveStats(readArchiveSetDir, archiveGid, backend):
    try:
        stats = archiveStats(readArchiveSetDir, archiveGid, backend)
        stats.writeStats(backend)
        if sys.version_info.major == 2:
            encodingDict = {"mode": "wb"}
        else:
            encodingDict = { "mode":"w",  "encoding": "utf-8" }
        if not os.path.exists("stats"):
            os.makedirs("stats")
        with open("stats/" + archiveGid + ".stats", **encodingDict) as f:
            jsonIndentF(stats.toDict(), f)
    except:
        logging.exception("With archiveGid %s", archiveGid)

def archiveDirGen(archiveDirFile):
    with open(archiveDirFile, encoding="utf-8") as f:
        while True:
            line = f.readline()
            if not line: break
            line
            archiveDir = line.strip()
            if archiveDir:
                yield archiveDir

def potentialSemiconductor(formulaDict):
    """simplistic classification of semiconductors"""
    rows = {
        "I": ["H","Li","Na","K","Rb","Cs","Fr"],
        "II": ["Be","Mg","Ca","Sr","Ba", "Ra"],
        "III": ["B","Al","Ga","In","Tl"],
        "IV": ["Si","Ge"], # "C","Sn","Pb"],
        "V": ["N","P","As","Sb","Bi"],
        "VI": ["O","S","Se","Te","Po"],
        "VII": ["F","Cl","Br","I","At"]
    }
    presentRows = {}
    unclassified = {}
    for elNr, nEl in formulaDict.items():
        el = chemical_symbols[elNr if elNr >= 0 else 0]
        classified = False
        for r, els in rows.items():
            if el in els:
                presentRows[r] = presentRows.get(r, 0) + nEl
                classified = True
                break
        if not classified:
            unclassified[el] = nEl
    if unclassified:
        return False
    unbalancedRows = dict(presentRows)
    if unbalancedRows.get("IV"):
        del unbalancedRows["IV"]
    if unbalancedRows.get("III", -2) == unbalancedRows.get("V",-1):
        del unbalancedRows["III"]
        del unbalancedRows["V"]
    if unbalancedRows.get("II", -2) == unbalancedRows.get("VI",-1):
        del unbalancedRows["II"]
        del unbalancedRows["VI"]
    if unbalancedRows:
        return False
    else:
        if presentRows:
            return True
        else:
            return False

def potentialMetal(formulaDict):
    """simplistic classification of metals"""
    metals = set([3,4,5, 11,12,13] + list(range(19,32)) + list(range(37,50)) + list(range(55,84)) + list(range(87,103)))
    for el in formulaDict.keys():
        if not el in metals:
            return False
    return True

def updateTopValues(topValues, k, v, nTopValues = 5):
    """Updates the most common values (i.e. the ones with largest v) contained in topValues with k,v"""
    if not topValues:
        topValues.append((v,[k]))
    elif v >= topValues[-1][0]:
        i = 0
        while v < topValues[i][0]:
            i += 1
        if topValues[i][0] == v:
            topValues[i][1].append(k)
        else:
            topValues.insert(i, (v,[k]))
            if len(topValues) > nTopValues:
                topValues.pop()

def isKey(k):
    return not (k.startswith("n_") or k.startswith("r_") or k in ['class'])

def isCounter(k):
    return k.startswith("n_") or k.startswith("r_")

def collectAll(tree, collectClass, fOut = None, collectedData = None):
    cData = collectedData if collectedData is not None else {}
    if tree.get("class") == collectClass:
        for k,v in tree.items():
            if isKey(k):
                oldV = cData.get(k)
                if not oldV:
                    oldV = {}
                    cData[k] = oldV
                for col in filter(isCounter, v.keys()):
                    oldV[col] = oldV.get(col, 0) + v[col]
    else:
        for k,v in tree.items():
            if isinstance(v, dict):
                collectAll(v, collectClass, fOut = None, collectedData = cData)
    if fOut:
        toCsv(cData,fOut)
    return cData

def collectFilter(tree, collectClass, filterClass, filterValue,  fOut = None, collectedData = None, kNow = None):
    cData = collectedData if collectedData is not None else {}
    cls = tree.get("class")
    if cls == collectClass:
        for k,v in tree.items():
            if isinstance(v, dict):
                if isKey(k):
                    kNow = k
                return collectFilter(v, collectClass, filterClass, filterValue, fOut, collectedData = cData, kNow = kNow)
    elif cls == filterClass:
        valToAdd = tree.get(filterValue, {})
        for col, v in valToAdd.items():
            if isCounter(col):
                oldV = cData.get(kNow)
                if not oldV:
                    oldV = {}
                    cData[kNow] = oldV
                oldV[col] = oldV.get(col, 0) + v
    else:
        for k,v in tree.items():
            if isinstance(v, dict):
                collectFilter(v, collectClass, filterClass, filterValue, fOut = None, collectedData = cData, kNow = kNow)
    if fOut:
        toCsv(cData,fOut)
    return cData

def flattenedTree(path, tree, f):
    t = tree
    for p in path.split("/"):
        if p:
            t = t[p]
    toCsv(t, f)

def toCsv(tree, f):
    cols = set()
    vals = []
    for k,v in tree.items():
        if k and isKey(k):
            vals.append(k)
            cols.update(filter(isCounter, v.keys()))
    cols = list(sorted(cols))
    f.write("name")
    for col in cols:
        f.write(",")
        f.write(col)
    f.write("\n")
    for v in sorted(vals):
        f.write(str(v))
        f.write(',')
        vv = tree[v]
        for col in cols:
            f.write(str(vv.get(col,0)))
            f.write(",")
        f.write("\n")

def mergeTree(t1, t2):
    for k,v in t2.items():
        oldV = t1.get(k)
        if isinstance(v,dict):
            if oldV is None:
                oldV = {}
                t1[k] = oldV
            mergeTree(oldV, v)
        elif isinstance(v, set):
            if oldV is None:
                t1[k] = set(v)
            else:
                oldV.update(v)
        elif isinstance(v, int) or isinstance(v, float):
            if oldV is None:
                t1[k] = v
            else:
                t1[k] = oldV + v
        elif isinstance(v, type("")) or isinstance(v, type(u"")):
            if oldV is None:
                t1[k] = v
        else:
            raise Exception("non mergeable type %s found when merging %s and %s" % (type(v), t1, t2))

def mergeCounters(d1, d2):
    for k, v in d2.items():
        d1[k] = d1.get(k, 0) + v

def computeSlimTree(tree):
    """computes a slimmed down tree with also all partial counters"""
    res = {}
    counters = {}
    myCounters = {}
    for k,v in tree.items():
        if k == "formulaIds":
            counters["n_geo"] = counters.get("n_geo", 0) + sum(map(len, v.values()))
            counters["n_compositions"] = counters.get("n_compositions", 0) + len(v)
        elif isinstance(v, dict):
            res[k], c = computeSlimTree(v)
            mergeCounters(counters, c)
        elif isinstance(v, set) or isinstance(v, list):
            counters["n_" + k] = counters.get("n_" + k, 0) + len(v)
        elif isinstance(v, int) or isinstance(v, float):
            myCounters[k] = v
        elif isinstance(v, type("")) or isinstance(v, type(u"")):
            res[k] = v
        else:
            raise Exception("Unexpected type %s found when computing slim tree %s -> %s" % (type(v), k, v))
    myCounters.update(counters)
    res.update(counters)
    return (res, myCounters)

def toHighLevelStats(stats):
    formulasHist = {}
    formulas = stats.get('formulas',{})
    topFormulas = []
    formulasRHist = {}
    rFormulasHist = {}
    rTopFormulas = []
    formulasR = stats.get('formulasR',{})
    topFormulasR = []
    configIds = stats.get('configIds',{})
    topFormulasE = []
    configHist = {}
    topConfigs = []
    elementUsageR = {}
    mostUsedElements = []
    formulaExpansions = {}
    metalsF = set()
    nMetals = 0
    nMetalsR = 0
    metalClass = {}
    nonMetalClass = {}
    nElementalMetals = 0
    nElementalMetalsR = 0
    nBinaryAlloy = 0
    nBinaryAlloyR = 0
    metalEHist = {}
    mostExploredMetal = []
    rMetalEHist = {}
    rMostExploredMetal = []
    semiconductorsF = set()
    nSemiconductors = 0
    nSemiconductorsR = 0
    semiEHist = {}
    mostExploredSemiconductors = []
    rSemiEHist = {}
    rMostExploredSemiconductors = []
    for formula, nSinglePoint in formulas.items():
        formulasHist[nSinglePoint] = formulasHist.get(nSinglePoint, 0) + 1
        updateTopValues(topFormulas, formula, nSinglePoint)
        nRun = formulasR[formula]
        formulasRHist[nRun] = formulasRHist.get(nRun, 0) + 1
        updateTopValues(topFormulasR, formula, nRun)
        fDict = formulaToDict(formula)
        for el, nEl in fDict.items():
            elementUsageR[el] = elementUsageR.get(el, 0) + nRun
        nrs = list(fDict.values())
        if nrs:
            gcd = nrs[0]
            for n in nrs[1:]:
                gcd = fractions.gcd(gcd, n)
        else:
            gcd = 1
        rDict = {}
        els = list(fDict.items())
        els.sort(key = lambda x: x[0])
        rFormula = ""
        for el, nEl in els:
            rFormula += el
            if nEl > gcd:
                rFormula += str(nEl // gcd)
        expansions = formulaExpansions.get(rFormula)
        if expansions is None:
            formulaExpansions[rFormula] = set([formula])
        else:
            expansions.add(formula)
        if potentialMetal(fDict):
            metalsF.add(rFormula)
            nMetalsR += nRun
            nMetals += nSinglePoint
            nEl = len(fDict)
            metalClass[0] = metalClass.get(0,0) + nSinglePoint
            metalClass[nEl] = metalClass.get(nEl,0) + nSinglePoint
            if (len(fDict) == 1):
                nElementalMetals += nSinglePoint
                nElementalMetalsR += nRun
            elif (len(fDict) == 2):
                nBinaryAlloy += nSinglePoint
                nBinaryAlloyR += nRun
        else:
            nEl = len(fDict)
            nonMetalClass[0] = nonMetalClass.get(0,0) + nSinglePoint
            nonMetalClass[nEl] = nonMetalClass.get(nEl,0) + nSinglePoint
        if potentialSemiconductor(fDict):
            semiconductorsF.add(rFormula)
            nSemiconductorsR += nRun
            nSemiconductors += nSinglePoint
    for formula, structIds in configIds.items():
        if not formula:
            continue
        for structId, nSingleP in structIds.items():
            configHist[nSingleP] = configHist.get(nSingleP, 0) + 1
            updateTopValues(topConfigs, formula + ":" + structId, nSingleP)
        updateTopValues(topFormulasE, formula, len(structIds))
    for el, nUses in elementUsageR.items():
        updateTopValues(mostUsedElements, el, nUses)
    for rFormula, expandedF in formulaExpansions.items():
        nExp = sum(map(lambda f: formulas[f], expandedF))
        updateTopValues(rTopFormulas, rFormula, nExp)
        rFormulasHist[nExp] = rFormulasHist.get(nExp, 0) + 1
    for rFormula in semiconductorsF:
        #nExp = sum(map(lambda f: len(configIds[f]), formulaExpansions[rFormula]))
        nExp = sum(map(lambda f: formulas[f], formulaExpansions[rFormula]))
        updateTopValues(rMostExploredSemiconductors, rFormula, nExp)
        rSemiEHist[nExp] = rSemiEHist.get(nExp, 0) + 1
    for rFormula in semiconductorsF:
        for f in formulaExpansions[rFormula]:
            nExp = formulas[f]
            updateTopValues(mostExploredSemiconductors, f, nExp)
            semiEHist[nExp] = semiEHist.get(nExp, 0) + 1
    for rFormula in metalsF:
        #nExp = sum(map(lambda f: len(configIds[f]), formulaExpansions[rFormula]))
        nExp = sum(map(lambda f: formulas[f], formulaExpansions[rFormula]))
        updateTopValues(rMostExploredMetal, rFormula, nExp)
        rMetalEHist[nExp] = rMetalEHist.get(nExp, 0) + 1
    for rFormula in metalsF:
        for f in formulaExpansions[rFormula]:
            nExp = formulas[f]
            updateTopValues(mostExploredMetal, f, nExp)
            metalEHist[nExp] = metalEHist.get(nExp, 0) + 1
    res = {}
    for k, v in stats.items():
        if not (k in nestedTypes or k in doublyNested):
            res[k] = v
    res["metalClass"] = metalClass
    res["nonMetalClass"] = nonMetalClass
    res["elements"] = res.get("elements")
    res["XC_functional_type"] = stats.get("XC_functional_type", {})
    res["classify"] = stats.get("classify", {})
    res["formulasHist"] = formulasHist
    res['nFormulas'] = len(formulas)
    res['nRFormulas'] = len(formulaExpansions)
    res["topFormulas"] = topFormulas
    res["rFormulasHist"] = rFormulasHist
    res["rTopFormulas"] = rTopFormulas
    res["formulasRHist"] = formulasRHist
    res["topFormulasR"] = topFormulasR
    res["topFormulasE"] = topFormulasE
    res["configHist"] = configHist
    res["topConfigs"] = topConfigs
    res["nConfigs"] = sum(map(len, configIds.values()))
    res["elementUsageR"] = elementUsageR
    res["mostUsedElements"] = mostUsedElements
    res["nMetals"] = nMetals
    res["nMetalsR"] = nMetalsR
    res["nElementalMetals"] = nElementalMetals
    res["nElementalMetalsR"] = nElementalMetalsR
    res["nBinaryAlloy"] = nBinaryAlloy
    res["nBinaryAlloyR"] = nBinaryAlloyR
    res["metalEHist"] = metalEHist
    res["mostExploredMetal"] = mostExploredMetal
    res["rMetalEHist"] = rMetalEHist
    res["rMostExploredMetal"] = rMostExploredMetal
    res["nSemiconductors"] = nSemiconductors
    res["nSemiconductorsR"] = nSemiconductorsR
    res["semiEHist"] = semiEHist
    res["mostExploredSemiconductors"] = mostExploredSemiconductors
    res["rSemiEHist"] = rSemiEHist
    res["rMostExploredSemiconductors"] = rMostExploredSemiconductors
    return res

if __name__ == '__main__':
    usage = """usage: {basename} [--nproc n] [--help] [--archive-gid <archive_gid>] [--archive-gid-file file/with/archiveGids] [--archive-dir path/to/archive/dir] [--combine-stats stats1 ... statsN] [--to-high-level-stats statsFile1 ... statsFileN]
    """.format(basename = os.path.basename(sys.argv[0]))
    iarg = 1
    nproc = 1
    archiveGids = []
    archiveDir = "/parsed/production"
    while iarg < len(sys.argv):
        arg = sys.argv[iarg]
        iarg += 1
        if arg == "--nproc":
            if iarg < len(sys.argv):
                nproc = int(sys.argv[iarg])
                iarg += 1
            else:
                login = ("Error, expected the number of processors after --proc", usage)
                sys.exit(1)
        elif arg == "--archive-gid":
            if iarg < len(sys.argv):
                archiveGids.append(sys.argv[iarg])
                iarg += 1
            else:
                print("Error, expected archive dir file after --archive-dir-file", usage)
                sys.exit(1)
        elif arg == "--archive-uri":
            if iarg < len(sys.argv):
                m = re.match("nmd://[AR]([-a-zA-Z0-9_]{28})/?", sys.argv[iarg])
                if not m:
                    print("Error, expected an archive uri, but got %s" % sys.argv[iarg])
                    sys.exit(1)
                archiveGids.append("R" + m.group(1))
                iarg += 1
            else:
                print("Error, expected archive dir file after --archive-dir-file", usage)
                sys.exit(1)
        elif arg == "--archive-gid-file":
            if iarg < len(sys.argv):
                archiveGids.extend(open(sys.argv[iarg]).readlines())
                if nproc > 1:
                    from multiprocessing import Pool
                    p = Pool(nproc)
                    for i in p.imap_unordered(saveArchiveStats, archiveDirGen(archiveDirFile)):
                        pass
                else:
                    for archiveDir in archiveDirGen(archiveDirFile):
                        saveArchiveStats(archiveDir)
                iarg += 1
            else:
                print("Error, expected archive dir file after --archive-dir-file", usage)
                sys.exit(1)
        elif arg == "--archive-dir":
            if iarg < len(sys.argv):
                archiveDir = sys.argv[iarg]
                iarg += 1
            else:
                print("Error, expected archive dir after --archive-dir", usage)
        elif arg == '--help':
            print(usage)
            sys.exit(0)
        elif arg == '--combine-stats':
            allStats = StatsInfo("")
            statsToCombine = sys.argv[iarg:]
            iarg = len(sys.argv)
            for statsF in statsToCombine:
                try:
                    newStats = json.load(open(statsF, encoding="utf-8"))
                    allStats.addStats(StatsInfo.fromDict(newStats))
                except:
                    logging.exception("failure with %r", statsF)
            jsonIndentF(allStats.toDict(), sys.stdout)
        elif arg == '--to-high-level-stats':
            statsToConvert = sys.argv[iarg:]
            iarg = len(sys.argv)
            for fPath in statsToConvert:
                with open(fPath, encoding="utf-8") as fIn:
                    stats = json.load(fIn)
                statsHL = toHighLevelStats(stats)
                with open(fPath + "_hl", "w", encoding="utf-8") as fOut:
                    jsonIndentF(statsHL, fOut)
        else:
            print("Error, unexpected argument ", arg, ",", usage)
            sys.exit(1)

    if archiveGids:
        #if nproc > 1:
        #    from multiprocessing import Pool
        #    p = Pool(nproc)
        #    for i in p.imap_unordered(saveArchiveStats, archiveDirGen(archiveDirFile)):
        #        pass
        #else:
            backend = JsonParseEventsWriterBackend(None)
            for archiveGid in archiveGids:
                saveArchiveStats(archiveDir, archiveGid, backend)
